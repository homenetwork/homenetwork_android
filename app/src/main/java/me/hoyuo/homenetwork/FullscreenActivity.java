package me.hoyuo.homenetwork;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

public class FullscreenActivity extends Activity {
    int SPLASH_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                overridePendingTransition(0, android.R.anim.fade_in);
                finish();
            }
        }, SPLASH_TIME);
    }
}