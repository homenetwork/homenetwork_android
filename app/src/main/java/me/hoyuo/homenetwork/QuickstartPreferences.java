package me.hoyuo.homenetwork;

public class QuickstartPreferences {
    public static final String REGISTRATION_READY = "registrationReady";
    public static final String REGISTRATION_GENERATING = "registrationGenerating";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_UI_UPDATE = "gcmuiupdtae";
}
