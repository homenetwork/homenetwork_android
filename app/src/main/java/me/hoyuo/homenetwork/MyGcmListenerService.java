package me.hoyuo.homenetwork;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

public class MyGcmListenerService extends GcmListenerService {

    private static final String LOG_TAG = MyGcmListenerService.class.getSimpleName();

    /**
     * @param from SenderID 값을 받아온다.
     * @param data Set형태로 GCM으로 받은 데이터 payload이다.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String title = data.getString("title");
        String message = data.getString("message");
        String custom_key1 = data.getString("custom_key1");
        String custom_key2 = data.getString("custom_key2");

        Log.d(LOG_TAG, "From: " + from);
        Log.d(LOG_TAG, "Title: " + title);
        Log.d(LOG_TAG, "Message: " + message);

        // GCM으로 받은 메세지를 디바이스에 알려주는 sendNotification()을 호출한다.
        sendNotification(title, message);
    }


    /**
     * 실제 디바에스에 GCM으로부터 받은 메세지를 알려주는 함수이다. 디바이스 Notification Center에 나타난다.
     *
     * @param title
     * @param message
     */
    private void sendNotification(String title, String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_action_settings_remote)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (title.equals("gas")) {
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } else if (title.equals("motionDetection")) {
            notificationManager.notify(1 /* ID of notification */, notificationBuilder.build());
        } else if (title.equals("power")) {
            notificationManager.notify(2 /* ID of notification */, notificationBuilder.build());
        } else if (title.equals("door")) {
            notificationManager.notify(3 /* ID of notification */, notificationBuilder.build());
        } else if (title.equals("dimm")) {
            notificationManager.notify(4 /* ID of notification */, notificationBuilder.build());
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(QuickstartPreferences.GCM_UI_UPDATE));
    }
}