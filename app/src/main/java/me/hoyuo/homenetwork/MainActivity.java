package me.hoyuo.homenetwork;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = MainActivity.class.getSimpleName();

    private Button mRegistrationButton;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    SwitchCompat doorLock_SwitchCompat;
    SwitchCompat power_SwitchCompat;
    SwitchCompat dimm_SwitchCompat;
    AppCompatTextView dimm_level;
    int dimm_level_value;

    private BroadcastReceiver gcmReceiver = null;

    public void getInstanceIdToken() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    public void registBroadcastReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();

                if (action.equals(QuickstartPreferences.REGISTRATION_READY)) {
                    // 액션이 READY일 경우
                    mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                    mInformationTextView.setVisibility(View.GONE);
                } else if (action.equals(QuickstartPreferences.REGISTRATION_GENERATING)) {
                    // 액션이 GENERATING일 경우
                    mRegistrationProgressBar.setVisibility(ProgressBar.VISIBLE);
                    mInformationTextView.setVisibility(View.VISIBLE);
                    mInformationTextView.setText(getString(R.string.registering_message_generating));
                } else if (action.equals(QuickstartPreferences.REGISTRATION_COMPLETE)) {
                    // 액션이 COMPLETE일 경우
                    mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                    mRegistrationButton.setText(getString(R.string.registering_message_complete));
                    mRegistrationButton.setEnabled(false);
                    String token = intent.getStringExtra("token");
                    mInformationTextView.setText(token);
                } else if (action.equals(QuickstartPreferences.GCM_UI_UPDATE)) {
                    UpdateUI();
                }

            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registBroadcastReceiver();

        startActivity(new Intent(this, FullscreenActivity.class));

        doorLock_SwitchCompat = (SwitchCompat) findViewById(R.id.doorLock);
        doorLock_SwitchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                HTTPRequest httpRequest;
                if (isChecked) {
                    httpRequest = new HTTPRequest();
                    httpRequest.execute("door", "on");
                } else {
                    httpRequest = new HTTPRequest();
                    httpRequest.execute("door", "off");
                }
            }
        });

        power_SwitchCompat = (SwitchCompat) findViewById(R.id.power);
        power_SwitchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                HTTPRequest httpRequest;
                if (isChecked) {
                    httpRequest = new HTTPRequest();
                    httpRequest.execute("power", "on");
                } else {
                    httpRequest = new HTTPRequest();
                    httpRequest.execute("power", "off");
                }
            }
        });

        dimm_SwitchCompat = (SwitchCompat) findViewById(R.id.dimm);
        dimm_SwitchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                HTTPRequest httpRequest;
                if (isChecked) {
                    httpRequest = new HTTPRequest();
                    httpRequest.execute("dimm", "on");
                    dimm_level_value = 1;
                    dimm_level.setText(dimm_level_value + "");
                } else {
                    httpRequest = new HTTPRequest();
                    httpRequest.execute("dimm", "off");
                    dimm_level_value = 20;
                    dimm_level.setText(dimm_level_value + "");

                }
            }
        });

        dimm_level = (AppCompatTextView) findViewById(R.id.dimm_level);
        dimm_level_value = 20;
        dimm_level.setText(dimm_level_value + "");

        AppCompatButton dimmUp = (AppCompatButton) findViewById(R.id.dimmup);
        dimmUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dimm_level_value > 1) {
                    dimm_level_value--;
                    dimm_level.setText(dimm_level_value + "");
                    HTTPRequest httpRequest = new HTTPRequest();
                    httpRequest.execute("dimm", "up");
                }
            }
        });

        AppCompatButton dimmDown = (AppCompatButton) findViewById(R.id.dimmdown);
        dimmDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dimm_level_value < 20) {
                    dimm_level_value++;
                    dimm_level.setText(dimm_level_value + "");
                    HTTPRequest httpRequest = new HTTPRequest();
                    httpRequest.execute("dimm", "down");
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_READY));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_GENERATING));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.GCM_UI_UPDATE));
        UpdateUI();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void UpdateUI() {
        WebTask webTask = new WebTask();
        webTask.execute();
        Log.d("TEST LOG", "UpdateUI");
    }

    public class WebTask extends AsyncTask<Void, Void, boolean[]> {
        private final String LOG_TAG = WebTask.class.getSimpleName();


        private boolean[] getDataFromJson(String data) throws JSONException {
            Log.d(LOG_TAG, "getDataFromJson Call");
            boolean[] ret = new boolean[3];
            ret[0] = ret[1] = ret[2] = false;
            String[] dataForm = {"power", "dimm", "doorLock"};

            if (data == null)
                return ret;

            JSONArray dataJson = new JSONArray(data);

            for (int i = 1; i < 4; i++) {
                JSONObject object = dataJson.getJSONObject(i);
                ret[i - 1] = object.getBoolean(dataForm[i - 1]);
            }

            dimm_level_value = dataJson.getJSONObject(4).getInt("dimmLevel");

            return ret;
        }

        @Override
        protected boolean[] doInBackground(Void... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String data = null;
            try {
                URL url = new URL("http://hoyuo.me:3000");

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    data = null;
                }
                data = buffer.toString();

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                return getDataFromJson(data);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(boolean[] ret) {
            super.onPostExecute(ret);

            power_SwitchCompat.setChecked(ret[0]);
            dimm_SwitchCompat.setChecked(ret[1]);
            doorLock_SwitchCompat.setChecked(ret[2]);
            dimm_level.setText(dimm_level_value + "");
        }
    }

    public class HTTPRequest extends AsyncTask<String, Void, Void> {
        private final String LOG_TAG = HTTPRequest.class.getSimpleName();

        @Override
        protected Void doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            try {
                String uri = "http://hoyuo.me:3000";

                uri = uri + "/" + params[0] + "/" + params[1];

                URL url = new URL(uri);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();

                Log.i(LOG_TAG, "URL" + uri);
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }
    }
}
